<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 21/07/2018
 * Time: 17:20
 */

namespace BeardedByte;


class Logger {

    static $log_file_path = __DIR__.'/../../log.txt';

    static function logMessage($message) {
        $output = ['type' => 'log', 'time' => date('Y/m/d H:i:s'), 'content' => $message];
        file_put_contents(Logger::$log_file_path, json_encode($output).PHP_EOL, FILE_APPEND);
    }

    static function logException(\Exception $e) {
        $output = ['type' => 'log', 'time' => date('Y/m/d H:i:s'), 'content' => array(
            'request' => $_SERVER['REQUEST_URI'],
            'file' => $e->getFile(),
            'code' => $e->getCode(),
            'line' => $e->getLine(),
            'message' => $e->getMessage()
        )];
        file_put_contents(Logger::$log_file_path, json_encode($output).PHP_EOL, FILE_APPEND);
    }
}