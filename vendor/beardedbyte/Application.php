<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 12/07/2018
 * Time: 14:13
 */

namespace BeardedByte;


use Klein\Klein;

class Application {

    /**
     * @var Container
     */
    public $container;

    /**
     * @var Klein
     */
    public $router;

    /**
     * @var \Twig_Environment
     */
    public $twig;

    /**
     * @var array
     */
    public $controllers;

    /**
     * Application constructor.
     */
    public function __construct() {
        $this->container = new Container($this);
        $this->router = $this->container->router;
        $this->twig = $this->container->twig;

        $this->router->onHttpError([$this, 'onHttpError']);
    }

    /**
     * @param $name
     * @param Controller $controller
     *
     * @return $this
     * @throws \Exception
     */
    public function add_controller($name, Controller $controller) {
        if (array_key_exists($name, $this->controllers)) {
            throw new \Exception('A controller is already registred as "'.$name.'"');
        }
        $this->controllers[$name] = $controller;
        return $this;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function isEnv($name) {
        return ($this->container->config->get('ENV', 'PROD') == $name);
    }

    /**
     * @param \Exception $e
     */
    public function onException(\Exception $e) {
        $this->onHttpError(500);
    }

    /**
     * @param int $code
     */
    public function onHttpError($code) {
        echo sprintf('<h1 style="text-align: center">Erreur %d</h1>', $code);
    }

    /**
     * @param string $template
     * @param array $params
     */
    public function render($template, $params=[]) {
        echo $this->container->twig->render($template, $params);
    }

    /**
     *
     */
    public function run() {
        try {
            $this->router->dispatch();
        } catch (\Exception $e) {
            $this->onException($e);
        }
    }
}