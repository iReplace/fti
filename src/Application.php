<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 02/11/2018
 * Time: 18:38
 */

namespace FTI;


use BeardedByte\Logger;
use FTI\Controler\API;
use FTI\Controler\Redaction;
use FTI\Controler\Showcase;
use FTI\Model\Dessin;
use FTI\Model\Edition;
use FTI\Model\User;
use Klein\Request;
use Klein\Response;

class Application extends \BeardedByte\Application {

    /**
     * @var null|array
     */
    public $connectedUser;

    /**
     * @var bool
     */
    public $isUserConnected;

    /**
     * @var bool
     */
    public $isUserAdmin;

    public function __construct() {
        parent::__construct();

        // Creation de la connexion à la base de données
        $this->container
            ->add_database('database', new \PDO('sqlite:/'.__DIR__.'/../private/database.db'))
        ;

        // Creation des modèles de données
        $this->container
            ->add_modelmanager('user', new User($this->container->get_database('database')))
            ->add_modelmanager('edition', new Edition($this->container->get_database('database')))
            ->add_modelmanager('dessin', new Dessin($this->container->get_database('database')))
        ;


        // Récupération, le cas échéant, de l'utilisateur connecté
        $user_id = isset($_SESSION['user']) ? $_SESSION['user']['id'] : null;
        $this->connectedUser = $this->container->get_modelmanager('user')->get($user_id);

        if ($this->connectedUser) {
            $this->isUserConnected = true;
            $this->isUserAdmin = boolval($this->connectedUser['admin']);
        } else {
            $this->isUserConnected = false;
            $this->isUserAdmin = false;
        }

        // Ajout à twig des infos sur l'utilisateur
        $this->twig->addGlobal('connected_user', $this->connectedUser);

        // Redirection automatique vers la version https
        if ($this->isEnv('PROD')) {
            $this->router->respond('*', function (Request $req, Response $rep) {
                if (!$req->isSecure()) {
                    $rep->redirect('https://fti.jeanba.fr'.$req->pathname());
                    $rep->send();
                    exit();
                }
            });
        }

        // Creation des controllers
        $this
            ->add_controller('Showcase', new Showcase($this))
            ->add_controller('Redaction', new Redaction($this))
            ->add_controller('API', new API($this))
        ;

    }

    public function onException(\Exception $e) {
        Logger::logException($e);
        if ($this->isEnv('DEV')) {
            $this->render('dev/error.html.twig', array(
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ));
        } else {
            $this->onHttpError(500);
        }
    }

    public function onHttpError($code) {

        if ($this->isUserConnected) {
            Logger::logMessage(['title' => "Erreur HTTP ($code)", 'request' => $_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'], 'mail' => $this->connectedUser['email']]);
        }

        switch ($code) {
            case 200:

                break;
            case 401:
                $ct = $this->router->request()->headers()->get('Accept');

                $params_text = "";
                $first = true;
                foreach ($this->redirectParams as $key => $value) {
                    if ($first) {
                        $params_text .= '?';
                        $first = false;
                    } else {
                        $params_text .= '&';
                    }
                    $params_text .= urlencode($key).'='.urlencode($value);
                }

                if ($ct == 'application/json') {
                    echo json_encode(array(
                        'status' => 'error',
                        'code' => 401,
                        'messageText' => 'Vous n\'êtes pas connecté',
                        'messageHTML' => '<p>Vous n\'êtes pas connecté <a class="button button-red" href="/connexion'.$params_text.'">Se connecter</a></p>',
                        'payload' => null
                    ));
                } else {
                    echo $this->twig->render('http/401.html.twig', array('params_text' => $params_text));
                }
                break;

            case 403:
                $ct = $this->router->request()->headers()->get('Accept');
                if ($ct == 'application/json') {
                    echo json_encode(array(
                        'status' => 'error',
                        'code' => 403,
                        'messageText' => 'Vous n\'avez pas le droit d\'accéder à cette ressource',
                        'messageHTML' => '<p>Vous n\'avez pas le droit d\'accéder à cette ressource</p>',
                        'payload' => null
                    ));
                } else {
                    echo $this->twig->render('http/403.html.twig', array());
                }
                break;

            case 404:
                $ct = $this->router->request()->headers()->get('Accept');
                if ($ct == 'application/json') {
                    echo json_encode(array(
                        'status' => 'error',
                        'code' => 404,
                        'messageText' => 'La ressource demandée n\'existe pas',
                        'messageHTML' => '<p>La ressource demandée n\'existe pas</p>',
                        'payload' => null
                    ));
                } else {
                    echo $this->twig->render('http/404.html.twig', array());
                }
                break;


            default:
                $ct = $this->router->request()->headers()->get('Accept');
                if ($ct == 'application/json') {
                    echo json_encode(array(
                        'status' => 'error',
                        'code' => $code,
                        'messageText' => 'Erreur: '.$code,
                        'messageHTML' => '<p>Erreur: '.$code.'</p>',
                        'payload' => null
                    ));
                } else {
                    echo $this->twig->render('http/XXX.html.twig', array('code' => $code));
                }
        }
    }
}