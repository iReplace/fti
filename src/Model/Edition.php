<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 02/11/2018
 * Time: 18:39
 */

namespace FTI\Model;


use BeardedByte\ModelManager;

class Edition extends ModelManager {

    public function __construct(\PDO $database) {
        parent::__construct($database, 'edition', true);
    }

    public function get_last() {
        $sql = "SELECT * FROM edition WHERE publique=1 ORDER BY annee DESC, mois DESC LIMIT 1";
        $req = $this->database->prepare($sql);
        $req->execute(array());
        $editions = $req->fetchAll(\PDO::FETCH_ASSOC);
        if (count($editions) > 0) {
            return $editions[0];
        }
        return null;
    }

    public function ordered_by_date() {
        $sql = "SELECT * FROM edition WHERE publique=1 ORDER BY annee DESC, mois DESC";
        $req = $this->database->prepare($sql);
        $req->execute(array());
        return $req->fetchAll(\PDO::FETCH_ASSOC);
    }

}