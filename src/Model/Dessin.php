<?php
/**
 * Created by PhpStorm.
 * User: jeanbaptistecaplan
 * Date: 02/11/2018
 * Time: 18:39
 */

namespace FTI\Model;


use BeardedByte\ModelManager;

class Dessin extends ModelManager {

    public function __construct(\PDO $database) {
        parent::__construct($database, 'dessin', true);
    }

    public function ordered_by_date() {
        $sql = "SELECT * FROM dessin WHERE publique=1 ORDER BY date_publication DESC";
        $req = $this->database->prepare($sql);
        $req->execute(array());
        return $req->fetchAll(\PDO::FETCH_ASSOC);
    }

}