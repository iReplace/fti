BASE_URL = "/api/redaction/utilisateurs";
var container = $('.container');
var error = $('#user-error');
var list = $('#user-list');
var form = $('#user-form');
$('#user-plus').on('click', function(event) {
    event.preventDefault();
    container.children().hide();
    $('.topbar > span.title').text('Ajouter un utilisateur');
    //form.find('h2').text('Ajouter un utilisateur');
    form.find('#user-send').text('Ajouter');
    form.find('#user-delete').hide();
    form.find('input').value('');
    form.find('#user-admin').removeAttr('checked');
    form.find('#mode').value('insert');
    form.show();

});
$('.user-edit').on('click', function(event) {
    event.preventDefault();
    event.element.createSpinner();
    $.API.get(BASE_URL+'/'+event.element.attr('href'), {}, function(json) {
        event.element.removeSpinner();
        if (json.type == 'success') {
            $('.topbar > span.title').text('Modifier un utilisateur');
            //form.find('h2').text('Modifier un utilisateur');
            form.find('#user-send').text('Modifier');
            form.find('#user-delete').show();
            form.find('input').value('');
            form.find('input').value(json.payload);
            form.find('#mode').value('update');
            $('#mode').value('update');
            container.children().hide();
            form.show();
        } else {
            error.html(json.messageHTML);
            error.show();
        }
    });
});
$('#user-send').on('click', function(event) {
    event.preventDefault();
    event.element.createSpinner();
    var data = form.find('input').value();
    $.API.post(BASE_URL + ((data.id.length > 0) ? '/'+data.id : ''), data, function(json) {
        event.element.removeSpinner();
        if (json.type == 'success') {
            form.find('#user-send').width(form.find('#user-send').width());
            form.find('#user-send').html('<span class="text-success"><i class="fal fa-check"></i></span>');
            window.location.reload();
        } else {
            error.html(json.messageHTML);
            error.show();
        }
    });
});
$('#user-delete').on('click', function(event) {
    event.preventDefault();
    if (window.confirm('Êtes-vous sur de vouloir supprimer cet utilisateur ?')) {
        event.element.createSpinner();
        var data = form.find('input').value();
        $.API.delete(BASE_URL + ((data.id.length > 0) ? '/'+data.id : ''), function(json) {
            event.element.removeSpinner();
            if (json.type == 'success') {
                form.find('#user-delete').width(form.find('#user-send').width());
                form.find('#user-delete').html('<span class="text-success"><i class="fal fa-check"></i></span>');
                window.location.reload();
            } else {
                error.html(json.messageHTML);
                error.show();
            }
        });
    }
});
$('#user-cancel').on('click', function(event) {
    event.preventDefault();
    container.children().hide();
    $('.topbar > span.title').text('Liste des utilisateurs');
    list.show();
});